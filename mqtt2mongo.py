import os
import datetime
import json
import traceback

from pymongo import MongoClient

from lib.mqtt import MqttBroker

import logging
log_handler = logging.StreamHandler()
log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_handler.setFormatter(log_formatter)
root_logger = logging.getLogger()
root_logger.addHandler(log_handler)
root_logger.setLevel(logging.DEBUG)
log = logging.getLogger(__name__)


class MessageProcessor:
    def __init__(self, collection, subscriptions):
        self.broker = None
        self.collection = collection
        self.subscriptions = subscriptions

    
        
    def store_message(self, client, userdata, message):
        log.info(f"message received on topic:{message.topic}")
        db_record = {
            "topic" : message.topic,
            "message" : message.payload.decode('utf-8'),
            "received" : datetime.datetime.utcnow()
        }
        try :
            self.collection.insert_one(db_record).inserted_id
        except :
            log.error('Unexpected exception when inserting data into DB')
            log.error(traceback.format_exc())

    def set_broker(self, broker):
        self.broker = broker
            
    def start(self, dummy) :
        """Callback after mqtt is connected"""
        log.info("Now subscribing")
        broker.set_default_cb(self.store_message)
        try:
            for sub in subscriptions :
                self.broker.subscribe(sub, self.store_message)
        except:
            log.error("error during mqtt subscription")
            log.error(traceback.format_exc())
    

# If PARAMS env variable exists take configuration from it, in json format
# otherwise, suppose that the configuration is given directly in the environment
params = os.environ.get('PARAMS')
if params :
    params = json.loads(params.replace("'", '"'))
else :
    params = dict(os.environ)
    params['subscriptions'] = eval(params['subscriptions'])

print(params)
mqtt_server =  params["mqtt_server"]
mqtt_port =  int(params["mqtt_port"])
cafile =  params.get("cafile")   
crtfile =  params.get("crtfile")
keyfile =  params.get("keyfile")

mongo_uri = params["mongo_uri"]
mongo_db_name = params["mongo_db_name"]
mongo_server = MongoClient(mongo_uri)
db = mongo_server[mongo_db_name]
collection = db.messages

subscriptions = params["subscriptions"]
if (not type(subscriptions) == list):
    raise Exception("subscriptions should be a list")

processor = MessageProcessor(collection, subscriptions)
broker = MqttBroker(mqtt_server, mqtt_port, processor.start)
processor.set_broker(broker)
if (cafile) : broker.ca_file = cafile
if (crtfile) : broker.crt_file = crtfile
if (keyfile) : broker.key_file = keyfile

broker.start()
broker.join()

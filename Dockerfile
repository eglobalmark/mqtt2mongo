FROM python:3.7-alpine3.10

RUN mkdir /app
ADD requirements.txt /app
RUN pip install -r /app/requirements.txt

ADD lib/ /app/lib
ADD mqtt2mongo.py /app

WORKDIR /app

VOLUME /secrets

CMD ["python", "mqtt2mongo.py"]